# Candler Aggregator - Interview Challenge

version 1.2.0 - 21/02/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/candle-aggregator.svg)](http://bitbucket.org/ivan-sanabria/candle-aggregator/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/candle-aggregator.svg)](http://bitbucket.org/ivan-sanabria/candle-aggregator/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_candle-aggregator)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_candle-aggregator)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_candle-aggregator&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_candle-aggregator)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_candle-aggregator)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_candle-aggregator&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_candle-aggregator)

## Specifications
Please review the section **Aggregated-Price History retrieval** of the README file under specifications' folder.

## Requirements
- Golang 1.19.x
- Serverless 2.72.x

## Serverless Setup

In order to deploy the application in AWS account is required to have an **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is
required to execute the following commands on the root folder of the project on a terminal:

```bash
    npm install -g serverless
    serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID} --secret ${AWS_SECRET_ACCESS_KEY} --profile demos
```

## Check Application Test Coverage

To verify test coverage on terminal:

1. Verify the version of Go - 1.19.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    go test ./... -coverprofile=coverage.out
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of Go - 1.19.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    go install  github.com/google/go-licenses
    go-licenses csv ./...
```

## Deployment on AWS using Serverless

After configuring serverless and see the test coverage, let's proceed to deploy the application in
AWS Lambda to transform quotes into candles:

```bash
    mkdir bin
    GOARCH=amd64 GOOS=linux go build -o bin ./...
    serverless deploy -v --aws-profile demos
```

## Execute Lambda in AWS Console

After deployment is successful, you can log in into the AWS console and execute the lambda with payload event:

```json
    {
      "recover": "1637275980"
    }
```

Recover attribute is UTC unix time to transform the records from specific path. The given value reads the data
from the S3 path 2021/11/18/22/53/.

# Contact Information

Email: icsanabriar@googlemail.com
