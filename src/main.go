// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package main implements the function to create candle sticks based on last marker and list of quotes.
package main

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/candle"
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/quote"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"os"
	"strconv"
	"time"
)

// getS3Client retrieves a new instance of S3 client to read objects from S3 bucket.
func getS3Client() *s3.S3 {

	s3session, _ := session.NewSession(&aws.Config{
		Region: aws.String(os.Getenv("REGION"))},
	)

	return s3.New(s3session)
}

// getDynamoDBClient retrieves a new instance of DynamoDB client to persist candles.
func getDynamoDBClient() *dynamodb.DynamoDB {

	dynamodbSession := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	return dynamodb.New(dynamodbSession)
}

// LambdaHandler implements the function that call the quote and candle implementations to transform quotes into candles.
func LambdaHandler(event dto.Event) (dto.Response, error) {

	start := time.Now()
	recoverTime := event.Recover

	marker, err := strconv.ParseInt(recoverTime, 10, 64)

	if err != nil {
		log.Fatalf("Error reading given recover time %s: %s", recoverTime, err)
	}

	bucket := aws.String(os.Getenv("CANDLE_BUCKET"))
	s3Client := getS3Client()

	contents, err := quote.ListQuotes(marker, bucket, s3Client)

	if err != nil {
		log.Fatalf("Unable to list quotes in bucket %q with marker %d: %s", *bucket, marker, err)
	}

	log.Printf("Found %d items in bucket %q with marker %d.", len(contents), *bucket, marker)

	quotes, err := quote.ParseQuotes(bucket, contents, s3Client)

	if err != nil {
		log.Fatalf("Unable to parse quotes from contents %d: %s", len(contents), err)
	}

	log.Printf("Found %d quotes in %d contents.", len(quotes), len(contents))

	candles, err := candle.GenerateCandles(quotes)

	if err != nil {
		log.Printf("Unable to generate candles: %s", err)
	}

	log.Printf("Generate %d candles with given quotes.", len(candles))

	table := aws.String(os.Getenv("CANDLE_TABLE"))
	dynamodbClient := getDynamoDBClient()

	persisted := candle.PersistCandles(candles, table, dynamodbClient)

	elapsed := time.Since(start)
	log.Printf("Storage of %d candles was successful and function elapsed runtime %s.", persisted, elapsed)

	return dto.Response{
		Message: fmt.Sprintf("Event received %s persisted %d candles and took %s.", event.Recover, persisted, elapsed),
	}, nil
}

// main function that trigger the lambda in order to aggregate candles into candles.
func main() {
	lambda.Start(LambdaHandler)
}
