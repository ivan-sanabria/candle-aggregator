// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package candle responsible to provide logic to group quotes by isin and generate the candle for specific time.
package candle

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"time"
)

// FORMAT is the layout of the Quote reception field.
const FORMAT = "2006-01-02T15:04:05.999999"

// initCandle implements the logic to initialize a candle instance.
func initCandle(quote dto.Quote, reception int64) dto.Candle {

	return dto.Candle{
		Isin:       quote.Isin,
		OpenPrice:  quote.Price,
		OpenTime:   reception,
		HighPrice:  quote.Price,
		LowPrice:   quote.Price,
		ClosePrice: quote.Price,
		CloseTime:  reception,
	}
}

// updateCandle implements the logic to update the candle using the given quote.
func updateCandle(candle dto.Candle, quote dto.Quote, reception int64) dto.Candle {

	candle.ClosePrice = quote.Price
	candle.CloseTime = reception

	if candle.LowPrice > quote.Price {
		candle.LowPrice = quote.Price
	}

	if candle.HighPrice < quote.Price {
		candle.HighPrice = quote.Price
	}

	return candle
}

// GenerateCandles implements the logic to transform dto.Quote array into a map[string]dto.Candle.
func GenerateCandles(quotes []dto.Quote) (map[string]dto.Candle, error) {

	candles := make(map[string]dto.Candle)

	for _, quote := range quotes {

		parsedTime, err := time.Parse(FORMAT, quote.Reception)

		if err != nil {
			return nil, err
		}

		reception := parsedTime.Unix()

		if val, ok := candles[quote.Isin]; ok {
			candles[quote.Isin] = updateCandle(val, quote, reception)
		} else {
			candles[quote.Isin] = initCandle(quote, reception)
		}
	}

	return candles, nil
}
