// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package candle responsible to provide logic to group quotes by isin and generate the candle for specific time.
package candle

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// TestGenerateCandles implements the test case to validate the logic to transform dto.Quote array into a map[string]dto.Candle.
func TestGenerateCandles(t *testing.T) {

	openReception := "2021-11-02T15:00:00.000000"
	openTime, _ := time.Parse(FORMAT, openReception)

	closeReception := "2021-11-02T15:00:30.000000"
	closeTime, _ := time.Parse(FORMAT, closeReception)

	expected := map[string]dto.Candle{
		"1": {
			Isin:       "1",
			OpenTime:   openTime.Unix(),
			OpenPrice:  1.0,
			HighPrice:  1.8,
			LowPrice:   0.5,
			ClosePrice: 1.8,
			CloseTime:  closeTime.Unix(),
		},
	}

	quotes := []dto.Quote{
		{
			Isin:      "1",
			Price:     1.0,
			Reception: "2021-11-02T15:00:00.000000",
		},
		{
			Isin:      "1",
			Price:     0.5,
			Reception: "2021-11-02T15:00:01.000000",
		},
		{
			Isin:      "1",
			Price:     1.8,
			Reception: "2021-11-02T15:00:30.000000",
		},
	}

	results, err := GenerateCandles(quotes)

	assert.Nil(t, err)
	assert.Equal(t, results, expected)
}

// TestParseReceptionError implements the test case to fail when reception is in wrong format.
func TestParseReceptionError(t *testing.T) {

	quotes := []dto.Quote{
		{
			Isin:      "1",
			Price:     1.0,
			Reception: "WRONG FORMAT",
		},
	}

	results, err := GenerateCandles(quotes)

	assert.NotNil(t, err)
	assert.Nil(t, results)
}
