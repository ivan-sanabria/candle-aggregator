// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package candle responsible to provide logic to group quotes by isin and generate the candle for specific time.
package candle

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"log"
)

// PersistCandles implements the logic to persist the given map[string]dto.Candle into dynamoDB.
func PersistCandles(candles map[string]dto.Candle, table *string, client dynamodbiface.DynamoDBAPI) int {

	persisted := 0

	for _, candle := range candles {

		item, _ := dynamodbattribute.MarshalMap(candle)

		input := &dynamodb.PutItemInput{
			Item:      item,
			TableName: table,
		}

		_, err := client.PutItem(input)

		if err != nil {
			log.Printf("Error calling PutItem with isin %s: %s", candle.Isin, err)
		} else {
			persisted++
		}
	}

	return persisted
}
