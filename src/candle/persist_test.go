// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package candle responsible to provide logic to group quotes by isin and generate the candle for specific time.
package candle

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// mockDynamodbClient Defines a mock struct to use as dynamo client in unit tests.
type mockDynamodbClient struct {
	dynamodbiface.DynamoDBAPI
}

// PutItem is mock function used to unit tests.
func (m *mockDynamodbClient) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {

	result := dynamodb.PutItemOutput{}

	if input.TableName == nil || *input.TableName == "" {
		return &result, errors.New("missing required field PutItemInput.TableName")
	}

	if input.Item == nil {
		return &result, errors.New("missing required field PutItemInput.Item")
	}

	return &result, nil
}

// TestPersistCandles implements the test case to validate the logic to persist the given map[string]dto.Candle into dynamoDB.
func TestPersistCandles(t *testing.T) {

	client := &mockDynamodbClient{}
	table := aws.String("candles-test")
	reception := "2021-11-02T15:00:00.000000"
	receptionTime, _ := time.Parse(FORMAT, reception)

	candles := map[string]dto.Candle{
		"1": {
			Isin:       "1",
			OpenTime:   receptionTime.Unix(),
			OpenPrice:  1.0,
			HighPrice:  1.8,
			LowPrice:   0.9,
			ClosePrice: 1.5,
			CloseTime:  receptionTime.Unix(),
		},
		"2": {
			Isin:       "2",
			OpenTime:   receptionTime.Unix(),
			OpenPrice:  3.0,
			HighPrice:  4.8,
			LowPrice:   1.9,
			ClosePrice: 2.5,
			CloseTime:  receptionTime.Unix(),
		},
		"3": {
			Isin:       "3",
			OpenTime:   receptionTime.Unix(),
			OpenPrice:  10.0,
			HighPrice:  112.8,
			LowPrice:   5.9,
			ClosePrice: 55.9,
			CloseTime:  receptionTime.Unix(),
		},
	}

	expected := len(candles)

	result := PersistCandles(candles, table, client)
	assert.Equal(t, result, expected)
}

// TestPutItemError implements the test case to fail when table is not given and given candles are not persisted.
func TestPutItemError(t *testing.T) {

	client := &mockDynamodbClient{}
	reception := "2021-11-02T15:00:00.000000"
	receptionTime, _ := time.Parse(FORMAT, reception)

	candles := map[string]dto.Candle{
		"1": {
			Isin:       "1",
			OpenTime:   receptionTime.Unix(),
			OpenPrice:  1.0,
			HighPrice:  1.8,
			LowPrice:   0.9,
			ClosePrice: 1.5,
			CloseTime:  receptionTime.Unix(),
		},
	}

	expected := 0

	result := PersistCandles(candles, nil, client)
	assert.Equal(t, result, expected)
}
