// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package dto defines the data transfer objects used to generate candles based on store quoted.
package dto

// Event is given in aws console to trigger manually the lambda.
type Event struct {
	Recover string `json:"recover"`
}

// Response is given on the aws console after lambda execution.
type Response struct {
	Message string `json:"message"`
}
