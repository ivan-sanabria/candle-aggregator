// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package dto defines the data transfer objects used to generate candles based on store quoted.
package dto

// Candle is a struct to transfer the data from history prices to candle.
type Candle struct {
	Isin       string  `json:"isin"`
	OpenTime   int64   `json:"openTime"`
	OpenPrice  float64 `json:"openPrice"`
	HighPrice  float64 `json:"highPrice"`
	LowPrice   float64 `json:"lowPrice"`
	ClosePrice float64 `json:"closePrice"`
	CloseTime  int64   `json:"closeTime"`
}

// Quote is a struct that is stored in S3 bucket.
type Quote struct {
	Isin      string  `json:"isin"`
	Price     float64 `json:"price"`
	Reception string  `json:"reception"`
}
