// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package quote implements the logic to read the data from S3 and parse to quote dto.
package quote

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"bytes"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/stretchr/testify/assert"
	"io"
	"testing"
)

// MockBucket is local map to simulate S3 bucket in unit tests.
type MockBucket map[string][]byte

// mockS3Client Defines a mock struct to use as s3 client in unit tests.
type mockS3Client struct {
	s3iface.S3API
	data map[string]MockBucket
}

// GetObject is mock function used on unit tests.
func (m *mockS3Client) GetObject(input *s3.GetObjectInput) (*s3.GetObjectOutput, error) {

	bucket := m.data[*input.Bucket]

	if object, ok := bucket[*input.Key]; ok {

		body := io.NopCloser(bytes.NewReader(object))
		output := s3.GetObjectOutput{
			Body: body,
		}

		return &output, nil

	} else {
		return nil, errors.New("missing key at get object function")
	}
}

// TestParseQuotes implements the test case to validate the logic to transform the json objects into dto.Quote.
func TestParseQuotes(t *testing.T) {

	bucket := "quotes-test"

	mockData := map[string]MockBucket{
		bucket: {
			"2021/11/21/15/04/05-1.json": []byte("{\"isin\":\"1\",\"price\":10.59,\"reception\":\"2021-11-21T15:04:05.89321\"}"),
			"2021/11/21/15/04/12-2.json": []byte("{\"isin\":\"2\",\"price\":19.24,\"reception\":\"2021-11-21T15:04:12.64805\"}"),
		},
	}

	client := &mockS3Client{data: mockData}

	contents := []*s3.Object{
		{
			Key: aws.String("2021/11/21/15/04/05-1.json"),
		},
		{
			Key: aws.String("2021/11/21/15/04/12-2.json"),
		},
	}

	expected := []dto.Quote{
		{
			Isin:      "1",
			Price:     10.59,
			Reception: "2021-11-21T15:04:05.89321",
		},
		{
			Isin:      "2",
			Price:     19.24,
			Reception: "2021-11-21T15:04:12.64805",
		},
	}

	results, err := ParseQuotes(&bucket, contents, client)

	assert.Nil(t, err)
	assert.Equal(t, results, expected)
}

// TestParseQuotesError implements the test case to fail when the s3 objects are retrieved.
func TestParseQuotesError(t *testing.T) {

	bucket := "quotes-test"

	contents := []*s3.Object{
		{
			Key: aws.String("2021/11/21/15/04/05-1.json"),
		},
	}

	client := &mockS3Client{}

	results, err := ParseQuotes(&bucket, contents, client)

	assert.NotNil(t, err)
	assert.Nil(t, results)
}

// TestParseQuotesParseError implements the test case to fail when the json file is not a dto.Quote.
func TestParseQuotesParseError(t *testing.T) {

	bucket := "quotes-test"

	mockData := map[string]MockBucket{
		bucket: {
			"2021/11/21/15/04/05-1.json": []byte("{\"isin\":\"1\",\"priception\"2021-11-21T15:04:05.89321\""),
		},
	}

	client := &mockS3Client{data: mockData}

	contents := []*s3.Object{
		{
			Key: aws.String("2021/11/21/15/04/05-1.json"),
		},
	}

	results, err := ParseQuotes(&bucket, contents, client)

	assert.NotNil(t, err)
	assert.Nil(t, results)
}
