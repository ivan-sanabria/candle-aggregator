// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package quote implements the logic to read the data from S3 and parse to quote dto.
package quote

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"time"
)

// getCurrentPrefix receives a marker (unix time) in order to generate the path to read items from S3 bucket.
func getCurrentPrefix(marker int64) string {

	t := time.Unix(marker, 0).UTC()

	return fmt.Sprintf("%04d/%02d/%02d/%02d/%02d/",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
	)
}

// ListQuotes implements the logic to list keys json that are present in specific minute S3 path.
func ListQuotes(marker int64, bucket *string, client s3iface.S3API) ([]*s3.Object, error) {

	prefix := aws.String(getCurrentPrefix(marker))

	resp, err := client.ListObjectsV2(
		&s3.ListObjectsV2Input{
			Bucket: bucket,
			Prefix: prefix,
		})

	if err != nil {
		return nil, err
	}

	return resp.Contents, nil
}
