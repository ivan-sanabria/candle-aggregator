// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package quote implements the logic to read the data from S3 and parse to quote dto.
package quote

import (
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/stretchr/testify/assert"
	"sort"
	"strings"
	"testing"
)

// ListObjectsV2 is mock function used to unit tests.
func (m *mockS3Client) ListObjectsV2(input *s3.ListObjectsV2Input) (*s3.ListObjectsV2Output, error) {

	bucket, ok := m.data[*input.Bucket]

	if !ok {
		return nil, errors.New("NoSuchBucket: The specified bucket does not exist")
	}

	var keys []string

	for key := range bucket {
		if strings.HasPrefix(key, *input.Prefix) {
			keys = append(keys, key)
		}
	}

	sort.Strings(keys)

	var contents []*s3.Object

	for _, key := range keys {

		object := s3.Object{
			Key: aws.String(key),
		}

		contents = append(contents, &object)
	}

	output := s3.ListObjectsV2Output{
		Contents:    contents,
		IsTruncated: aws.Bool(false),
	}

	return &output, nil
}

// TestListQuotes implements the test case to list the json keys that are present in specific minute S3 path.
func TestListQuotes(t *testing.T) {

	marker := int64(1637507045)
	bucket := "quotes-test"

	mockData := map[string]MockBucket{
		bucket: {
			"2021/11/21/15/04/05-1.json": []byte("{\"isin\":\"1\",\"price\":10.59,\"reception\":\"2021-11-21T15:04:05.89321\"}"),
		},
	}

	expected := []*s3.Object{
		{
			Key: aws.String("2021/11/21/15/04/05-1.json"),
		},
	}

	client := &mockS3Client{data: mockData}

	results, err := ListQuotes(marker, &bucket, client)

	assert.Nil(t, err)
	assert.Equal(t, results, expected)
}

// TestListQuotesError implements the test case to fail when the given bucket does not exist.
func TestListQuotesError(t *testing.T) {

	marker := int64(1637503445)
	bucket := "fake-bucket"

	client := &mockS3Client{}

	results, err := ListQuotes(marker, &bucket, client)

	assert.NotNil(t, err)
	assert.Nil(t, results)
}
