// Copyright 2021 Iván Camilo Sanabria
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package quote implements the logic to read the data from S3 and parse to quote dto.
package quote

import (
	"bitbucket.org/ivan-sanabria/candle-aggregator/src/dto"
	"bytes"
	"encoding/json"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
)

// ParseQuotes implements the logic to transform the json objects into dto.Quote.
func ParseQuotes(bucket *string, contents []*s3.Object, client s3iface.S3API) ([]dto.Quote, error) {

	quotes := make([]dto.Quote, 0)
	var quote dto.Quote

	for _, item := range contents {

		raw, err := client.GetObject(&s3.GetObjectInput{
			Bucket: bucket,
			Key:    item.Key,
		})

		if err != nil {
			return nil, err
		}

		buf := new(bytes.Buffer)
		_, _ = buf.ReadFrom(raw.Body)

		err = json.Unmarshal(buf.Bytes(), &quote)

		if err != nil {
			return nil, err
		}

		quotes = append(quotes, quote)
	}

	return quotes, nil
}
