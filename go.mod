module bitbucket.org/ivan-sanabria/candle-aggregator

go 1.13

require (
	github.com/aws/aws-lambda-go v1.27.0
	github.com/aws/aws-sdk-go v1.42.10
	github.com/google/go-licenses v0.0.0-20211006200916-ceb292363ec8 // indirect
	github.com/stretchr/testify v1.7.0
)
