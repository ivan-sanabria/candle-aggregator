#!/usr/bin/env bash

# Setup the package path of the application.
PACKAGE_PATH="${GOPATH}/src/bitbucket.org/${BITBUCKET_REPO_FULL_NAME}"
mkdir -pv "${PACKAGE_PATH}"

# Create bin folder at package path in order to store executable.
cd "${IMPORT_PATH}"
mkdir bin

# Retrieve module dependencies.
go get ./...
echo "Finished to get all dependencies."

# Run test cases with coverage.
go test ./... -coverprofile=coverage.out

# Build executable in order to deploy in aws lambda.
GOARCH=amd64 GOOS=linux go build -o bin ./...
echo "Finished building golang module."
